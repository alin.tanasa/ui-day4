function httpGet(theUrl) {
  var xmlHttp = new XMLHttpRequest();
  xmlHttp.open("GET", theUrl, false); // false for synchronous request
  xmlHttp.send(null);
  console.log(xmlHttp.responseText)
  return xmlHttp.responseText;
};

var data = httpGet("https://jsonplaceholder.typicode.com/users");

data = JSON.parse(data);

for (var i = 0; i < data.length; i++) {

  data[i]['id'] = data[i]['id'] * 2;
}

console.log(data);
